#include <iostream>
#include <vector>
#include <ctime>
#include <algorithm> 
#include <time.h>

using namespace std;

bool updateTopScores(const vector <int> &, vector <int> &);
bool updateTopScores_TwoLoops(const vector <int> &, vector <int> &);
bool testUpdateTopScore();
void printVec(const vector <int> &);
bool myCompare(int i, int j) { return i > j; } //Compare function for Sort() to sort in descending order
void CalcTime(int);
void CalcTime(int, vector <int> &, vector <int> &);


int main() {

	vector <int> sortedArr; //Sorted Scores

	vector <int> testArr = { 443, 439, 438, 438, 434, 418, 418, 417, 413, 411 };

	//Calling the test funciton
	if (testUpdateTopScore())
		cout << endl << endl << "New Algorithm with One Loop is working" << endl;
	else
		cout << endl << endl << "New Algorithm with One Loop isn't working" << endl;

	///// Time taken by One Loop

	clock_t begin = clock();

	updateTopScores(testArr, sortedArr);

	clock_t end = clock();
	double elapsed_secs = double(end - begin) / CLOCKS_PER_SEC;
	cout << "Time taken by single loop: " << elapsed_secs << endl;

	/// Time Taken by TWo Loops

	begin = clock();
	updateTopScores_TwoLoops(testArr, sortedArr);
	end = clock();

	elapsed_secs = double(end - begin) / CLOCKS_PER_SEC;
	cout << "Time taken by Two loop: " << elapsed_secs << endl;

	///

	vector <int> testSizes = { 10, 100, 1000, 10000 }; //Test Size

	for (int i = 0; i < testSizes.size(); i++)
		CalcTime(testSizes[i]); //Calling the Test function to Calculate Running time of Loops

	//Vectors to Store Running TIme
	vector <int> timeForOneLoop;
	vector <int> timeForTwoLoop;

	//Running the Test Algo 100 times for 10000 elements
	for (int i = 0; i < 100; i++)
		CalcTime(10000, timeForOneLoop, timeForTwoLoop);

	//Sorting 
	sort(timeForOneLoop.begin(), timeForOneLoop.end());
	sort(timeForTwoLoop.begin(), timeForTwoLoop.end());

	//Printing
	cout << "Best Single Loop Time: " << timeForOneLoop[0] << endl;
	cout << "Average Single Loop Time: " << timeForOneLoop[50] << endl;
	cout << "Worst Single Loop Time: " << timeForOneLoop[99] << endl;
	cout << "Best Double Loop Time: " << timeForTwoLoop[0] << endl;
	cout << "Average Single Loop Time: " << timeForTwoLoop[50] << endl;
	cout << "Worst Double Loop Time: " << timeForTwoLoop[99] << endl;

	system("pause");
	return 0;
}

//Print the Vector
void printVec(const vector <int> &x) {
	for (int i = 0; i < x.size(); i++)
		cout << x[i] << " ";
}


//Sort using one loop
bool updateTopScores(const vector<int> &in, vector<int> & out) {

	out = in;

	int flag = 0;

	for (int i = 0, j = 0; i < out.size() - 1; i++) {

		if (out[i + 1] > out[i])
			(swap(out[i], out[i + 1]), flag = 1);

		if (i == out.size() - 2 && flag == 1)
			flag = 0, i = -1;

	}


	return true;
}

//Sort using two loops
bool updateTopScores_TwoLoops(const vector<int> &in, vector<int> & out) {

	out = in;

	for (int i = 0; i < out.size() - 1; i++)
		for (int j = 0; j < out.size() - i - 1; j++)
			if (out[j] < out[j + 1])
				swap(out[j], out[j + 1]);


	return true;

}

//Test Function
bool testUpdateTopScore() {

	//Seed
	srand((int)time(NULL));

	//Declaring Vectors
	vector <int> myarray;
	vector <int> sortedArray;

	//Pushing Random Values into Vector
	for (int i = 0; i < 10; i++)
		myarray.push_back(rand() % 100 + 1);

	//Print Orignal Random Array
	cout << "Orignal Array \t";
	printVec(myarray);

	//Sorted using sort()
	sort(myarray.begin(), myarray.end(), myCompare);

	//Printing output
	cout << endl << endl << "Sorted Array \t";
	printVec(myarray);

	//Adding a new random value
	myarray.push_back(rand() % 100 + 1);

	//Printing output
	cout << endl << endl << "Added new Element at end \t";
	printVec(myarray);

	//Using updateTopScores()
	updateTopScores(myarray, sortedArray);

	//Printing output
	cout << endl << endl << "Sorted using updateTopScores() \t";
	printVec(sortedArray);

	//Sorting using sort() function again
	sort(myarray.begin(), myarray.end(), myCompare);

	//Printing output
	cout << endl << endl << "Sorted using Sort() \t";
	printVec(myarray);

	//Comparing if our sort function and built in sort function output has the same result
	return myarray == sortedArray;

}

//Calc running time
void CalcTime(int mysize) {

	vector<int> randVect;
	vector<int> OutputOne;
	vector<int> OutputTwo;
	clock_t clockBegin, clockEnd;

	//Filling the Vector

	for (int i = 0; i < mysize; i++)
		randVect.push_back(rand());

	clockBegin = clock(); //Start Time
	updateTopScores(randVect, OutputOne); //Run Loop
	clockEnd = clock(); //End time

	//Printing
	cout << "Time Elapsed by single Loop for " << mysize << " Element Vector: " <<
		(double(clockEnd - clockBegin)) / CLOCKS_PER_SEC << endl;

	clockBegin = clock(); //Start Time
	updateTopScores_TwoLoops(randVect, OutputTwo); //Run Loop
	clockEnd = clock(); //End time

	//Printing
	cout << "Time Elapsed by double Loop for " << mysize << " Element Vector: " <<
		(double(clockEnd - clockBegin)) / CLOCKS_PER_SEC << endl;

}

void CalcTime(int mysize, vector <int> &oneLoop, vector <int> &twoLoop) {

	vector<int> randVect;
	vector<int> OutputOne;
	vector<int> OutputTwo;
	clock_t clockBegin, clockEnd;

	//Filling the vector
	for (int i = 0; i < mysize; i++)
		randVect.push_back(rand());

	clockBegin = clock();
	updateTopScores(randVect, OutputOne);
	clockEnd = clock();

	oneLoop.push_back((double(clockEnd - clockBegin)) / CLOCKS_PER_SEC);

	cout << "Time Elapsed by single Loop for " << mysize << " Element Vector: " <<
		(double(clockEnd - clockBegin)) / CLOCKS_PER_SEC << endl;

	clockBegin = clock();
	updateTopScores_TwoLoops(randVect, OutputTwo);
	clockEnd = clock();

	twoLoop.push_back((double(clockEnd - clockBegin)) / CLOCKS_PER_SEC);

	cout << "Time Elapsed by double Loop for " << mysize << " Element Vector: " <<
		(double(clockEnd - clockBegin)) / CLOCKS_PER_SEC << endl;

}




